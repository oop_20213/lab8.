package com.khunpon.lab8;

public class Map {
    private int width;
    private int height;

    public Map(int width, int height) {
        this.width = width;
        this.height = height;

    }

    public Map() {
        this.width = 5;
        this.height = 5;
    }

    public void printMap() {
        for (int y = 1; y <= height; y++) {
            for (int x = 1; x <= width; x++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }
}
