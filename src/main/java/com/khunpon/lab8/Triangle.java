package com.khunpon.lab8;

public class Triangle {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double printTriangleArea() {
        double s = (a + b + c) / 2;
        double area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        System.out.println("Triangle area" + " = " + area);
        return area;
    }

    public double printTrianglePerimeter() {
        double perimeter = a + b + c;
        System.out.println("Triangle Perimeter" + " = " + perimeter);
        return perimeter;
    }
}
