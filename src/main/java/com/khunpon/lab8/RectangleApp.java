package com.khunpon.lab8;

public class RectangleApp {
    public static void main(String[] args) {
        Rectangle rect1 = new  Rectangle("rect1",10,5);
         rect1.printRectangleArea();
        rect1.printRectanglePerimeter();

        Rectangle rect2 = new  Rectangle("rect2",5,3);
        rect2.printRectangleArea();
        rect2.printRectanglePerimeter();  
    }
}